import requests
import json
from pymongo import MongoClient
from bson.objectid import ObjectId

quote_url = "https://finnhub.io/api/v1/quote?symbol={}&token=btft93n48v6rl6gbqd30"
trade_url = "http://localhost:8080/trade?ticker={}&state=COMPLETED"

db = MongoClient('mongodb://localhost:27017/')['tbb1_db']


def getStockData(ticker):
    try:
        r = requests.get(quote_url.format(ticker))
        if not r.text.startswith("You don't"):
            return r.json()

    except requests.RequestException as e:
        print("ERROR in price checker for stock: {}\nerror: {}\n------".format(ticker, e))
    
    return ""

def getPrice(ticker):
    price = 0.0
    data = getStockData(ticker)
    if data:
        price = data['c']
    return price

def populateDb():
    insert_list = []
    with open('default_stocks.json') as f:
        shares = json.load(f)
    for share in shares:
        price = getPrice(share['stockTicker'])
        insert_list.append({"sharePrice":price,
                            "companyName":share['companyName'],
                            "stockTicker":share['stockTicker']})
    db['shares'].insert_many(insert_list)

#checks if the share db is empty, if so it will populate it    
def checkDbSize():
    size = db['shares'].count_documents({})
    if size == 0:
        populateDb()

def updatePrices():
    sharesToUpdate = db['shares'].find()
    
    for share in sharesToUpdate:
        ticker = share['stockTicker']

        
        new_price = getPrice(ticker)
        db['shares'].update_one( {'_id' : ObjectId(share['_id'])},
                           {"$set": {"sharePrice" : new_price } }, upsert=False)
        print("Updated {}'s share price from ${} to ${}".format(share['companyName'], share['sharePrice'], new_price))


def big3prices():
    
    big3List = [
        {"stockTicker": "SPY","name":"S&P 500"},
        {"stockTicker": "DIA","name":"Dow Jones"},
        {"stockTicker": "IWM","name":"Rusell 2000"}
    ]
    
    for index in big3List:
        data = getStockData(index['stockTicker'])
        price = data['c']
        open_price = data['o']
        perc_change = 100 * ( price - open_price) / open_price
        index['price'] = price
        index['perc'] = round(perc_change, 2)

    return json.dumps(big3List)

def getPerformance(ticker):
    r = requests.get(trade_url.format(ticker))
    port_purchase_price = 0.0
    stock_quantity = 0.0
    money_made = 0.0
    for trade in r.json():
        if trade['type'] == 'BUY':
            port_purchase_price += (trade['stockQuantity'] * trade['price'])
            stock_quantity += (trade['stockQuantity'])
        elif trade['type'] == 'SELL':
            money_made += (trade['stockQuantity'] * trade['price'])
            stock_quantity -= (trade['stockQuantity'])       
    current_price = getPrice(ticker)
    port_worth = (stock_quantity * current_price) + money_made
    port_return = ((port_worth - port_purchase_price) / port_purchase_price) * 100
    return port_return

def getTotalPerformance():
    portfolio = db['portfolio'].find()
    ticker_count = 0.0
    total_return = 0.0
    for stock in portfolio:
        ticker_count += 1
        total_return += getPerformance(stock['stockTicker'])
    total_port_perf = total_return / ticker_count
    return total_port_perf


        
    

if __name__ == "__main__":
    # updatePrices()
    # populateDb()
    # checkDbSize()
    # big3prices()
    #getPerformance("DIS")
    getTotalPerformance()