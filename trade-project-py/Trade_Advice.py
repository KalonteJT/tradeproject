import pandas as pd 
from pandas_datareader import data as pdr
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import os


def tradeAdviceBollBands(inputStock):

    #User input of stock
    print(inputStock)

    now = datetime.now()
    start_date = '2017-01-01'
    today = now.strftime("%Y-%m-%d")
    print(today)

    #Load in data for inputStock from Yahoo Finance
    df_inputStock = pdr.DataReader(inputStock, 'yahoo', start_date, today)

    df_inputStock['30d mavg'] = df_inputStock['Close'].rolling(window=30).mean()
    df_inputStock['30d std'] = df_inputStock['Close'].rolling(window=30).std()


    df_inputStock['Top Upper Band'] = df_inputStock['30d mavg'] + (df_inputStock['30d std'] * 2)
    df_inputStock['Bottom Lower Band'] = df_inputStock['30d mavg'] - (df_inputStock['30d std'] * 2)
    df_inputStock['Upper Band'] = df_inputStock['30d mavg'] + (df_inputStock['30d std'] * 1)
    df_inputStock['Lower Band'] = df_inputStock['30d mavg'] - (df_inputStock['30d std'] * 1)


    #Plot the bollinger bands for inputStock

    cols = ['30d mavg','Top Upper Band','Upper Band', 'Bottom Lower Band','Lower Band', 'Close'] 
    today_less_100 = now - timedelta(days=100)
    today_less_100 = today_less_100.strftime("%Y-%m-%d")
    
    df_Boll = df_inputStock[cols][today_less_100: today] 
    
    plt.style.use('bmh')
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(111)

    x_axis = df_Boll.index.get_level_values(0)

    ax.fill_between(x_axis, df_Boll['Top Upper Band'], df_Boll['Upper Band'], color='green', alpha=0.3, label='Sell Zone')
    ax.fill_between(x_axis, df_Boll['Bottom Lower Band'], df_Boll['Lower Band'], color='green', alpha=0.3, label='Buy Zone')
    ax.fill_between(x_axis, df_Boll['Upper Band'], df_Boll['Lower Band'], color='yellow', alpha=0.3, label='Hold Zone')

    ax.plot(x_axis, df_Boll['Close'], color='black', lw=2, label='Close')
    ax.plot(x_axis, df_Boll['30d mavg'], color='blue', lw=2, label='30 Day Moving Avg')

    ax.legend(loc='lower right')
    ax.set_title('Bollinger Band Deviation for ' + inputStock)
    ax.set_xlabel('Date (Year/Month)')
    ax.set_ylabel('Price(USD)')

    fig.savefig(os.path.join(os.getcwd(),"static", "graph.png"))
   
    


    #Return Buy if in green band, Hold if in yellow band, Sell if in red band
    avg = (df_inputStock['High'][today] + df_inputStock['Low'][today]) / 2
   
    
    if avg >= df_inputStock['Upper Band'][today] and avg <= df_inputStock['Top Upper Band'][today]:
        advice = 'The stock price is currently in the sell zone. The stock is considered overbought: It is recommended you sell'
    elif avg >= df_inputStock['Bottom Lower Band'][today] and avg <= df_inputStock['Lower Band'][today]:
        advice = 'The stock price is currently in the buy zone. The stock is considered oversold: It is recommended you buy'
    elif avg >= df_inputStock['Lower Band'][today] and avg <= df_inputStock['Upper Band'][today]:
        advice = 'The stock price is currently in the hold zone. It is recommended you hold'
    elif avg > df_inputStock['Top Upper Band'][today]:
        advice = 'The stock price is more than 2 deviations above the moving average. High Volatility: sell with caution'
    elif avg < df_inputStock['Bottom Lower Band'][today]:
        advice = 'The stock price is more than 2 deviations below the moving average. High Volatility: buy with caution'
    else:
        advice = 'Undefined stock price: Not in zone'

    return advice, fig

    if __name__ == "__main__":
        tradeAdviceBollBands('AAPL')

