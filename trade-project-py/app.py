from flask import Flask, request, render_template, Response, send_file
from Trade_Advice import tradeAdviceBollBands
import io
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from apscheduler.schedulers.background import BackgroundScheduler
from prices import *
from flask_cors import CORS
import os

app = Flask(__name__)
CORS(app)

def test():
    print("hello")

@app.route('/advice/<ticker>', methods=['GET'])
def getStockAdvice(ticker):
    return tradeAdviceBollBands(ticker)[0]

@app.route('/advice/<ticker>/graph.png')
def graph(ticker):
    tradeAdviceBollBands(ticker)[1]
    return send_file(os.path.join(os.getcwd(),"static", "graph.png"))

@app.route('/big3',methods=['GET'])
def getBig3():
    return big3prices()

@app.route('/performance', methods=['GET'])
def getStockPerformance():
    return str(round(getTotalPerformance(),2))



if __name__ == "__main__":
    checkDbSize()
    scheduler = BackgroundScheduler()
    scheduler.add_job(updatePrices,'interval',minutes=3)
    scheduler.start()
    app.run(host="localhost", debug=True)






























