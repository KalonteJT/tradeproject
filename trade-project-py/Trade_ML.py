import pandas as pd 
from pandas_datareader import data as pdr
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
import numpy as np
from datetime import datetime, timedelta
from flask import Flask, request, render_template



#Function to use ML to predict buy/sell
def tradeMLGraph():
    #User input of stock and date
    inputStock = input('Enter stock ticker:')
    print(inputStock)

    now = datetime.now()
    start_date = '2017-01-01'
    end_date = now.strftime("%Y-%m-%d")

    #Load in data for inputStock from Yahoo Finance
    df_inputStock = pdr.DataReader(inputStock, 'yahoo', start_date, end_date)


    #create df to be used for prediction
    df_predict = df_inputStock[['Close']]

    #create new column: want to predict 30 days of close price
    future_days = 30
    df_predict['Prediction'] = df_predict['Close'].shift(-future_days)
    

    #independent variable 
    X = np.array(df_predict.drop(['Prediction'],1))[:-future_days]
    #dependent variable
    y = np.array(df_predict['Prediction'])[:-future_days]
    
  
    # 80/20 split
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    tree_model = DecisionTreeRegressor().fit(x_train, y_train)


    #the last rows of independent variable to test
    x_future = df_predict.drop(['Prediction'],1)[:-future_days]
    x_future = x_future.tail(future_days)
    x_future = np.array(x_future)

    #predict close prices with the tree model
    tree_predict = tree_model.predict(x_future)
    predictions = tree_predict
    #plot comparisons
    
    df_actual = df_predict[X.shape[0]:]
    df_actual['Predictions'] = predictions

    plt.figure(figsize=(16,8))
    plt.title('Close Price Prediciton for ' + inputStock)
    plt.xlabel('Date')
    plt.ylabel('Price(USD)')
    plt.plot(df_predict['Close'])
    plt.plot(df_actual[['Close', 'Predictions']])
    plt.legend(['Train', 'Val', 'Predictions'], loc='upper left')
    plt.show()

    


    # df_inputStock['30d mavg'] = df_inputStock['Close'].rolling(window=30).mean()
    # df_inputStock['30d std'] = df_inputStock['Close'].rolling(window=30).std()

    # df_inputStock['Upper Band'] = df_inputStock['30d mavg'] + (df_inputStock['30d std'] * 2)
    # df_inputStock['Lower Band'] = df_inputStock['30d mavg'] - (df_inputStock['30d std'] * 2)

    # X = df_inputStock[['30d mavg', '30d std']]['2018':'2020']
    # Y = df_inputStock[['Upper Band']]['2018':'2020']
    # Y2 = df_inputStock[['Lower Band']]['2018':'2020']

    # X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)
    # X2_train, X2_test, Y2_train, Y2_test = train_test_split(X, Y2, test_size=0.2)

    # UB_model = LinearRegression()
    # UB_model.fit(X_train, Y_train.values.ravel())

    # LB_model = LinearRegression()
    # LB_model.fit(X2_train, Y2_train.values.ravel())

    # print(X_test)
    # print(Y_test)


    # UB_prediction = UB_model.predict(X_test)
    # LB_prediction = LB_model.predict(X2_test)

    # plt.style.use('bmh')

    # fig = plt.figure(figsize=(18, 20))


    # axes1 = fig.add_axes([0.1, 0.1, 0.8, 0.8])#larger plot, actual
    # axes2 = fig.add_axes([0.18, 0.4, 0.3, 0.3])#smaller plot, predictions


    # #Actual band data
    # x1 = df_inputStock.index.date
    # y1 = df_inputStock['Upper Band']
    # y2 = df_inputStock['Lower Band']

    # #Predicted Upper band data
    # x2 = X_test.index
    # y3 = UB_prediction

    # #Predicted Lower band data
    # x3 = X2_test.index
    # y4 = LB_prediction

    # #Plot Date vs Price for all bands
    # axes1.plot(x1, y1, 'g', label='Upper Band')
    # axes1.plot(x1, y2, 'r', label = 'Lower Band')
    # axes2.plot(x2, y3, 'g', label = 'Upper Band Predictions')
    # axes2.plot(x3, y4, 'r', label = 'Lower Band Predictions')


    # #Labeling
    # axes1.legend(loc='lower right')
    # axes1.set_title('30 Day U/L Bollinger Bands for ' + inputStock)
    # axes1.set_xlabel('Date (Year)')
    # axes1.set_ylabel('Price(USD)')

    # plt.xticks(rotation=45)
    # axes2.set_title('Predictions')
    # axes2.legend(loc='upper left')
    # axes2.set_xlabel('Date (Year)')
    # axes2.set_ylabel('Price(USD)')

    # plt.show()

    

    return
tradeMLGraph()

