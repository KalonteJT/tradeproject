package com.conygre.training.tradesimulator.model;

public enum TradeState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private final String state;

    TradeState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    } 
}
