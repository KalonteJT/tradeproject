package com.citi.tradeapi.rest;

//import  org.junit.jupiter.api.extension.extendWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.citi.tradeapi.TradeapiApplication;
import com.citi.tradeapi.entities.Trade;

import com.citi.tradeapi.service.TradeService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TradeapiApplication.class)
public class StockControllerIntegrationTest {
   
    public static final ObjectId TEST_ID = new ObjectId("5f496e10bbc65a651e0ba095");


    @Autowired
    private TradeService service;

    
    @Test
    public void testFindById(){
        
        //given
        Trade testTrade = new Trade();
        List<Trade> testTrades = new ArrayList<Trade>();
        testTrades.add(testTrade);

        //when
        testTrade.setStockTicker("AAPL");
        testTrade.setPrice(100.0);
        testTrade.setId(TEST_ID);
        testTrade.setDateCreated(null);
        testTrade.setState(null);
        testTrade.setStockQuantity(2.0);


        //then
        assertThat(testTrade.getId(), equalTo(service.getTrade(TEST_ID.toString()).getId()));
    }

    @Test
    public void testGetAllStocks(){

        //given
        Iterable<Trade> testTrades = service.getAllStocks();
        Stream<Trade> stream = StreamSupport.stream(testTrades.spliterator(), false);

        //then
        assertThat(stream.count(), equalTo(1L));

    }
}
