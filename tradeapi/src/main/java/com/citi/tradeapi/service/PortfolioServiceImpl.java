package com.citi.tradeapi.service;

import com.citi.tradeapi.dto.PortfolioDto;
import com.citi.tradeapi.entities.Holding;
import com.citi.tradeapi.entities.Share;
import com.citi.tradeapi.entities.Trade;
import com.citi.tradeapi.repo.PortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PortfolioServiceImpl implements PortfolioService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PortfolioRepository portfolioRepository;

    @Autowired
    private TradeService tradeService;

    @Autowired
    private ShareService shareService;

    @Override
    public void addToPortfolio(Holding holding) {
        Optional<Holding> search = portfolioRepository.findByStockTicker(holding.getStockTicker());
        if( search.isPresent() )
        {
            Holding currentHolding = search.get();
            double currentAmount = currentHolding.getAmount();
            currentHolding.setAmount(currentAmount + holding.getAmount());
            portfolioRepository.save(search.get());
        }
        else
        {
            portfolioRepository.insert(holding);
        }

    }

    @Override
    public void removeFromPortfolio(Holding holding) {
        Optional<Holding> search = portfolioRepository.findByStockTicker(holding.getStockTicker());
        if( search.isPresent() )
        {
            double currentAmount = search.get().getAmount();
            if(currentAmount - holding.getAmount() >= 0)
            {
                search.get().setAmount(currentAmount - holding.getAmount());
                portfolioRepository.save(search.get());
            }

        }

    }

    @Override
    public void fufillTrade(Trade trade) {
        Optional<Holding> holding = portfolioRepository.findByStockTicker(trade.getStockTicker());

        if( trade.getType() == Trade.TradeType.BUY )
        {
            if ( holding.isPresent())
            {
                holding.get().addAmount(trade.getStockQuantity());
                portfolioRepository.save(holding.get());
            }
            else
            {
                Holding newHold = new Holding(trade.getStockQuantity(), trade.getStockTicker());
                portfolioRepository.save(newHold);
            }

        }
        else if ( trade.getType() == Trade.TradeType.SELL )
        {
            if( holding.isPresent() )
            {
                if( holding.get().getAmount() - trade.getStockQuantity() >= 0)
                {
                    holding.get().subAmount(trade.getStockQuantity());
                    portfolioRepository.save(holding.get());
                }
            }
        }
        trade.setState(Trade.TradeState.COMPLETED);
        tradeService.updateTrade(trade);

    }

    @Override
    public List<Trade> findTradesToBeExecuted() {
        Query query = new Query();
        Criteria stateCriteria = Criteria.where("state").is(Trade.TradeState.FILLED);
        query.addCriteria(stateCriteria);
        return mongoTemplate.find(query, Trade.class);
    }

    @Override
    public double getAssetValue(String stockTicker) {
        Optional<Share> share = shareService.getShare(stockTicker);
        Optional<Holding> holding = portfolioRepository.findByStockTicker(stockTicker);
        if(share.isPresent() && holding.isPresent())
        {
            return share.get().getSharePrice() * holding.get().getAmount();
        }

        return 0;
    }

    @Override
    public double getTotalAssetValue() {

        double total = 0;
        for(Holding hold : portfolioRepository.findAll())
        {
            total += getAssetValue(hold.getStockTicker());
        }

        return total;
    }

    @Override
    public List<PortfolioDto> getAllPortfolio() {
        List<PortfolioDto> portfolio = new ArrayList<>();

        List<Holding> holdings = portfolioRepository.findAll();
        for(Holding hold : holdings)
        {
            Optional<Share> share = shareService.getShare(hold.getStockTicker());
            if(share.isPresent())
            {
                portfolio.add(new PortfolioDto(share.get(), hold));
            }
        }


        return portfolio;
    }

    @Override
    public double getStockCount(String ticker) {
        Optional<Holding> hold = portfolioRepository.findByStockTicker(ticker);
        if(hold.isPresent())
        {
            System.out.println("found ticker: " + hold.get().getStockTicker());
            System.out.println("found amount: " + hold.get().getAmount());
            return hold.get().getAmount();
        }
        return 0;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:2000}")
    public void run()
    {
        List<Trade> trades = findTradesToBeExecuted();
        for( Trade trade:trades)
        {
            fufillTrade(trade);
        }

    }
}
