package com.citi.tradeapi.service;

import com.citi.tradeapi.dto.PortfolioDto;
import com.citi.tradeapi.entities.Holding;
import com.citi.tradeapi.entities.Trade;

import java.util.List;


//will look for trades that need to be executed and fufill that trade
public interface PortfolioService {
    void addToPortfolio(Holding holding);
    void removeFromPortfolio(Holding holding);
    void fufillTrade(Trade trade);
    List<Trade> findTradesToBeExecuted();
    double getAssetValue(String stockTicker);
    double getTotalAssetValue();
    List<PortfolioDto> getAllPortfolio();
    double getStockCount(String ticker);
}
