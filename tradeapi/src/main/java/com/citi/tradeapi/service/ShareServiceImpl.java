package com.citi.tradeapi.service;

import java.util.Collection;
import java.util.Optional;

import com.citi.tradeapi.entities.Share;
import com.citi.tradeapi.repo.ShareRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShareServiceImpl implements ShareService {


    @Autowired
    private ShareRepository repo;

    @Override
    public void addShare(Share share) {
        // TODO Auto-generated method stub
        repo.insert(share);

    }

    @Override
    public Collection<Share> getAllShares() {
        // TODO Auto-generated method stub
        return repo.findAll();
        //return null;
    }

    @Override
    public Optional<Share> getShare(String stockTicker) {
        return repo.findByStockTicker(stockTicker);
    }


    @Override
    public void updateShare(String stockTicker) {
        // TODO Auto-generated method stub

    }

    @Override
    public Collection<Share> queryTrade(String id, Double stockQuanity, Double price, String stockTicker,
            String status) {
        // TODO Auto-generated method stub
        return null;
    }


    
    
}
