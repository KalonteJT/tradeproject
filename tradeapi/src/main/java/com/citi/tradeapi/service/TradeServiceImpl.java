package com.citi.tradeapi.service;

import java.util.Collection;
import java.util.Optional;

import com.citi.tradeapi.entities.Trade;
import com.citi.tradeapi.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TradeRepository repo;

	@Override
	public void addTrade(Trade trade) {
        repo.insert(trade);
	}

    @Override
    public Collection<Trade> getAllStocks() {
        return repo.findAll();
    }

    @Override
    public Trade getTrade(String id){
        System.out.println(id);
        Optional<Trade> currentStock = repo.findById(new ObjectId(id));
        if(currentStock.isPresent()){
            return currentStock.get();
        }
        System.out.println("Not In Database");
        return null;
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    public void deleteTrade(String id) {
       repo.deleteById(new ObjectId(id));
    }

    @Override
    public Collection<Trade> queryTrade(String id, Double stockQuanity, Double price, String stockTicker, String status, String type) {
        Query dynamicQuery = new Query();
        if(id != null){
            System.out.println(id);
            Criteria nameCriteria = Criteria.where("id").is(id);
            dynamicQuery.addCriteria(nameCriteria);
        }
        // Change this later
        if(stockQuanity != null){
            System.out.println(stockQuanity);
            Criteria qCriteria = Criteria.where("stockQuantity").is(stockQuanity);
            dynamicQuery.addCriteria(qCriteria);
        }
        // Change this later
        if(price != null){
            System.out.println(price);
            Criteria pCriteria = Criteria.where("price").is(price);
            dynamicQuery.addCriteria(pCriteria);
        }
        if(stockTicker != null){
            System.out.println(stockTicker);
            Criteria tCriteria = Criteria.where("stockTicker").is(stockTicker);
            dynamicQuery.addCriteria(tCriteria);
        }
        if(status != null){
            System.out.println(status);
            Criteria sCriteria = Criteria.where("status").is(status);
            dynamicQuery.addCriteria(sCriteria);
        }
        if(type != null){
            System.out.println(type);
            Criteria sCriteria = Criteria.where("type").is(type);
            dynamicQuery.addCriteria(sCriteria);
        }
        Collection<Trade> result = mongoTemplate.find(dynamicQuery, Trade.class);
        return result;
    }
    

    @Override
    public void updateTrade(Trade trade) {
        repo.save(trade);
        return;

    }

    @Override
    public void cancelTrade(String id) {
        System.out.println("hello this the id: " + id + "||||");
        Trade trade = getTrade(id);
        if (trade != null)
        {
            if (trade.getState() == Trade.TradeState.CREATED)
            {
                trade.setState(Trade.TradeState.CANCELLED);
                updateTrade(trade);
            }

        }
    }

}