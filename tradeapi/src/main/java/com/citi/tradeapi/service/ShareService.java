package com.citi.tradeapi.service;

import java.util.Collection;
import java.util.Optional;

import com.citi.tradeapi.entities.Share;

public interface ShareService {
    Collection<Share> getAllShares();
    void addShare(Share share);
    Optional<Share> getShare(String stockTicker);
    void updateShare(String stockTicker);
    Collection<Share> queryTrade(String id, Double stockQuanity, Double price, String stockTicker, String status);
}
