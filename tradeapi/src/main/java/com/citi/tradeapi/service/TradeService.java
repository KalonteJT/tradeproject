package com.citi.tradeapi.service;

import java.util.Collection;

import com.citi.tradeapi.entities.Trade;

public interface TradeService {
    Collection<Trade> getAllStocks();
    void addTrade(Trade trade);
    Trade getTrade(String id);
    void deleteTrade(String id);
    Collection<Trade> queryTrade(String id, Double stockQuanity, Double price, String stockTicker, String status, String type);
    void updateTrade(Trade trade);
    void cancelTrade(String id);

}