package com.citi.tradeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TradeapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeapiApplication.class, args);
	}

}
