package com.citi.tradeapi.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Account{

    @Id
    private ObjectId id;
    private String name;
    private double earnings;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getEarnings() {
        return earnings;
    }

    public void setEarnings(double earnings) {
        this.earnings = earnings;
    }
}