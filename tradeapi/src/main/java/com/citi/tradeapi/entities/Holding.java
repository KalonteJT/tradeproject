package com.citi.tradeapi.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "portfolio")
public class Holding {

    @Id
    private ObjectId id;
    private String stockTicker;
    private double amount;

    public Holding(double amount, String stockTicker) {
        this.amount = amount;
        this.stockTicker = stockTicker;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void addAmount(double amount) {this.amount += amount; }

    public void subAmount(double amount) {this.amount -= amount; }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }
}
