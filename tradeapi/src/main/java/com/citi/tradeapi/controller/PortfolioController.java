package com.citi.tradeapi.controller;

import com.citi.tradeapi.dto.PortfolioDto;
import com.citi.tradeapi.service.PortfolioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class PortfolioController {
    @Autowired
    private PortfolioService portfolioService;

    @GetMapping("/portfolio/total")
    public double getTotalAssetValue()
    {

        return portfolioService.getTotalAssetValue();
    }

    @GetMapping("portfolio/count/{ticker}")
    public double getStockCount(@PathVariable("ticker") String ticker)
    {
        System.out.println("ticker: " + ticker);
        return portfolioService.getStockCount(ticker);
    }

    @GetMapping("/portfolio")
    public List<PortfolioDto> getPortfolio()
    {
        return portfolioService.getAllPortfolio();
    }



}
