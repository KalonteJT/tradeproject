package com.citi.tradeapi.controller;

import java.util.Collection;

import com.citi.tradeapi.entities.Share;
import com.citi.tradeapi.entities.Trade;
import com.citi.tradeapi.service.ShareService;
import com.citi.tradeapi.service.TradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")

public class TradeController {
    @Autowired
    private TradeService tradeService;

    @Autowired
    private ShareService shareService;

    @PostMapping("/trade")
    public void addStock(@RequestBody Trade trade){
        tradeService.addTrade(trade);
    }

    // @RequestMapping(path="/stock/{id}", method = RequestMethod.DELETE)
    // public void removeCar(@PathVariable("id") String id) {
    //     stockService.deleteStock(id);
    // }
    
//    @PostMapping(value = "/stock/cancel")
//    public void updateStock(@RequestBody String id){
//        stockService.cancelTrade(id);
//    }

    @PutMapping(value = "/trade/cancel/{id}")
    public void updateStock(@PathVariable("id") String id){
        tradeService.cancelTrade(id);
    }

    @GetMapping("/trade")
    public Collection<Trade> queryStock(
        @RequestParam(value = "id", required = false) String id,
        @RequestParam(value = "ticker", required = false) String ticker,
        @RequestParam(value = "price", required = false) Double price,
        @RequestParam(value = "quant", required = false) Double quant,
        @RequestParam(value = "date", required = false) String date,
        @RequestParam(value = "status", required = false) String status,
        @RequestParam(value = "type", required = false) String type
    ){
        return tradeService.queryTrade(id,quant,price,ticker,status,type);
    }

    @PostMapping("/share")
    public void addShare(@RequestBody Share share){
        shareService.addShare(share);
    }

    @GetMapping("/stocks")
    public Collection<Share> getShares()
    {
        return shareService.getAllShares();
    }
}