package com.citi.tradeapi.dto;


import com.citi.tradeapi.entities.Holding;
import com.citi.tradeapi.entities.Share;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PortfolioDto {
    private String stockTicker;
    private String companyName;
    private double stockPrice;
    private double amount;
    private double assetPrice;

    private PortfolioDto(String stockTicker, String companyName, double stockPrice, double amount) {
        this.stockTicker = stockTicker;
        this.companyName = companyName;
        this.stockPrice = stockPrice;
        this.amount = amount;
        BigDecimal bd = BigDecimal.valueOf(amount * stockPrice);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        this.assetPrice = bd.doubleValue();
    }

    public PortfolioDto(Share share, Holding hold)
    {
        this(share.getStockTicker(), share.getCompanyName(), share.getSharePrice(), hold.getAmount());
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public double getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(double stockPrice) {
        this.stockPrice = stockPrice;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAssetPrice() { return assetPrice; }

    public void setAssetPrice(double assetPrice) { this.assetPrice = assetPrice; }
}
