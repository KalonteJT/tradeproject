package com.citi.tradeapi.repo;


import com.citi.tradeapi.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, ObjectId>{
    
}