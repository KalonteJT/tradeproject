package com.citi.tradeapi.repo;

import com.citi.tradeapi.entities.Holding;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PortfolioRepository extends MongoRepository<Holding, ObjectId> {
    Optional<Holding> findByStockTicker(String stockTicker);

}
