package com.citi.tradeapi.repo;

import com.citi.tradeapi.entities.Share;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ShareRepository extends MongoRepository<Share, ObjectId>{
    Optional<Share> findByStockTicker(String stockTicker);
    
}
