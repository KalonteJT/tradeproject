#GET http://localhost:8080/trade?ticker=AAPL&price=233 HTTP/1.0

POST http://localhost:8080/trade HTTP/1.0
Content-Type: application/json

{
  "dateCreated": "2019-02-04",
  "stockTicker" : "INTL",
  "state" : "CREATED",
  "price" : "100",
  "stockQuantity" : "30",
  "type" : "BUY"
}

