import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TradeHistArray } from '../models/TradeHistArray';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Stocks } from '../models/Stocks';
import { Portfolio } from '../models/Portfolio';
import { TradeAdvice } from '../models/TradeAdvice';
import { Indice } from '../models/indice';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ConnectToApiService {

  getTradesUrl:string = 'http://localhost:8080/trade';
  getStocksUrl:string = 'http://localhost:8080/stocks';
  getPortfolioUrl:string = 'http://localhost:8080/portfolio';
  getTradeAdviceURL:string = 'http://localhost:5000/advice';
  getPerformanceURL:string = 'http://localhost:5000/performance';
  getBig3Url:string = 'http://localhost:5000/big3';
  getStockCountUrl:string = 'http://localhost:8080/portfolio/count/';

  constructor(private http:HttpClient) { }

  getTrades():Observable<TradeHistArray[]> {

    return this.http.get<TradeHistArray[]>(this.getTradesUrl);

  }

  getStocks():Observable<Stocks[]>
  {
    return this.http.get<Stocks[]>(this.getStocksUrl);
  }

  getStockCount(stockTicker){
    return this.http.get(this.getStockCountUrl+stockTicker,{responseType: 'text'});
  }

  makeTrade(data):Observable<any> {
    return this.http.post(this.getTradesUrl, data, httpOptions);
  }

  getPortfolio(): Observable<Portfolio[]> {
    return this.http.get<Portfolio[]>(this.getPortfolioUrl);
    //.pipe(
    //  retry(1),
    //  catchError(this.errorHandler)
    //);
  }

  getTradeAdvice(stockTicker) {
    return this.http.get(this.getTradeAdviceURL+'/'+stockTicker,{responseType: 'text'})
  }

  getTradeAdviceGraph(stockTicker){
    return this.http.get(this.getTradeAdviceURL+'/'+stockTicker+'graph.png', {responseType: 'blob'})
  }

  getTotalPortfolio(){
    return this.http.get(this.getPerformanceURL,{responseType: 'text'})
  }
  getBig3():Observable<Indice[]>

  {

    return this.http.get<Indice[]>(this.getBig3Url);

  }
  
  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
