// Model of what a trade looks like
export class TradeHistArray {
    dateCreated: string;
    stockTicker: string;
    state: string;
    price: number;
    stockQuantity: number;
    type: string; 
}