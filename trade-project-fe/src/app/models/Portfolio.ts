export class Portfolio {
    stockTicker: string;
    companyName: string; 
    stockPrice: number;
    amount: number;
    assetPrice: number;
}