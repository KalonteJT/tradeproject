import { Component, OnInit } from '@angular/core';
import { TradeHistArray } from '../../models/TradeHistArray';
import { ConnectToApiService } from '../../services/connect-to-api.service'

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css','../../app.component.css']
})
export class TableComponent implements OnInit {

  tradeHistArray: TradeHistArray[];
  textInput: string;
  filteredArray: TradeHistArray[];

  constructor(private tradeService:ConnectToApiService) { }

  ngOnInit(): void {
    /* get trade history from connect-to-api.service to populate the
      base model array, tradeHistArray. Also populate initial view of 
      of the changeable model, filteredArray */
    this.tradeService.getTrades().subscribe(trades => {
      this.tradeHistArray = trades;
      this.filteredArray = trades;
    });

    // Keep calling for updates to update base model array every 5 seconds
    window.setInterval(() =>  
      this.tradeService.getTrades().subscribe(trades => {
      this.tradeHistArray = trades;
    }), 5000);

  }

  // Filter through the changeable model array
  FilterHistory() {
    let transformInput = this.textInput.toUpperCase();
    let arrayToFilter = this.tradeHistArray;
    let newArray = arrayToFilter.filter(trade => 
      trade.state.includes(transformInput));

    this.filteredArray = newArray;
  }
  

}
