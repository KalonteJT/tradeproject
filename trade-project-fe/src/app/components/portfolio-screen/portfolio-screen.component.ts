/*app.component.ts*/
import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../../assets/canvasjs.min';
import { Observable } from 'rxjs';
import { ConnectToApiService } from '../../services/connect-to-api.service';
import { Portfolio } from '../../models/Portfolio';
import { NgForOf } from '@angular/common';

 
@Component({
  selector: 'app-portfolio-screen',
  templateUrl: './portfolio-screen.component.html',
  styleUrls: ['./portfolio-screen.component.css','../../app.component.css']
})
 

export class PortfolioScreenComponent implements OnInit {
  portfolio$: Observable<Portfolio[]>;
  portfolioEntries: Portfolio[];
  dataArray:any = [];
  portfolioTotal: number;


  constructor(private connectToApiService: ConnectToApiService) { 
  }
  ngOnInit() {
  // Sets the pie chart to update every 30 seconds
  this.loadPortfolio();
  window.setInterval(() =>  
  this.loadPortfolio(), 10000);

    }

	loadPortfolio(){
		this.dataArray.length = 0;
		this.connectToApiService.getPortfolio().subscribe(entry => {
		this.portfolioEntries = entry;
		this.populateChartValues();
		this.renderChart();

	});
	this.connectToApiService.getTotalPortfolio().subscribe(total => {
		this.portfolioTotal = Number(total);
	console.log(total);})
		console.log(this.portfolioTotal);
	}

	// Pushes the values obtained from the /portfolio route to a JS array for the chart data points
	private populateChartValues(){		
		for (let index = 0; index < this.portfolioEntries.length; index++) {
			this.dataArray.push({y: this.portfolioEntries[index].assetPrice,name: this.portfolioEntries[index].stockTicker});
		}
		
		
	}
	// renders the chart
	private renderChart(){
		console.log(this.dataArray);
		let chart = new CanvasJS.Chart("chartContainer", {
			theme: "light2",
			animationEnabled: true,
			exportEnabled: true,
			title:{
				text: "Portfolio Spread"
			},
			data: [{
				type: "pie",
				showInLegend: true,
				toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
				indexLabel: "{name} - #percent%",
				dataPoints: this.dataArray
			}]
		});
		chart.render();	
		
	}
}