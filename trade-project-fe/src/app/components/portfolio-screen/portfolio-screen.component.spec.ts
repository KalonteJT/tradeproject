import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioScreenComponent } from './portfolio-screen.component';

describe('PortfolioScreenComponent', () => {
  let component: PortfolioScreenComponent;
  let fixture: ComponentFixture<PortfolioScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
