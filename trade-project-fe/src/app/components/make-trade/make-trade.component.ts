import { Component, OnInit } from '@angular/core';
import { ConnectToApiService } from '../../services/connect-to-api.service'
import { Stocks } from '../../models/Stocks';

@Component({
  selector: 'app-make-trade',
  templateUrl: './make-trade.component.html',
  styleUrls: ['./make-trade.component.css', '../../app.component.css']
})
export class MakeTradeComponent implements OnInit {

  constructor(private tradeService:ConnectToApiService) { }

  ticker = '';
  chosenStock = '';
  chosenStockPrice = 0;
  quantity = 0;
  chosenQuantity = 0;
  choiceValue = '';

  tracker = {ticker:'',quant:-1};

  stockChoices: Stocks[];

  ngOnInit(): void {
    this.tradeService.getStocks().subscribe(stocks => {
      this.stockChoices = stocks;
    });
  }

  onStockChoiceChange(event) {
    this.ticker = this.stockChoices[event.target.value].stockTicker;
    this.chosenStock = this.stockChoices[event.target.value].companyName;
    this.choiceValue = this.stockChoices[event.target.value].companyName;
    this.chosenStockPrice = this.stockChoices[event.target.value].sharePrice;

      this.tradeService.getStockCount(this.ticker).subscribe(count => {
        if(this.tracker.ticker == this.ticker)
        {
          console.log("here");
          if(this.tracker.quant != Number(count))
          {
            this.chosenQuantity = this.tracker.quant
          }
          else{
            this.tracker = {ticker:'',quant:-1};
            this.chosenQuantity = parseInt(count,10);
          }
        }
        else{
          this.chosenQuantity = parseInt(count,10);

        }
      });
    
  }

  onChangeQuantity(event) {
    this.quantity = Number(event.target.value);
  }
  
  onMakeTrade() {
    // confirm that 'chosenStock' and 'chosenStockPrice' are filled.
    if (this.chosenStock === '' || this.quantity === 0) {
      window.alert("Please ensure you selected a stock and quantity");
    }
    else {
      // Ask if user is sure on total buy amount (stock price x quantity)
      let isCommfirmed = window.confirm(`Are you sure you want to buy ${this.quantity} ${this.chosenStock} stocks for $${this.chosenStockPrice}/stock?
      Total Price: $${this.chosenStockPrice*this.quantity}`);

      if (isCommfirmed) {
        // Create trade data
        const tradeData = {
          dateCreated: new Date(),
          stockTicker: this.ticker,
          state: 'CREATED',
          price: Number(this.chosenStockPrice),
          stockQuantity: this.quantity,
          type: 'BUY',
        }
      
        // Send newly created buy data to db
        console.log(tradeData);
        this.tradeService.makeTrade(tradeData).subscribe(res => {
          console.log("Buy submitted");
        })
    
      }
    }
  }

  onSellTrade()
  {
    const tradeData = {
      dateCreated: new Date(),
      stockTicker: this.ticker,
      state: 'CREATED',
      price: Number(this.chosenStockPrice),
      stockQuantity: this.quantity,
      type: 'SELL',
    }

    if(this.chosenQuantity - this.quantity >= 0)
    {
      let isCommfirmed = window.confirm(`Are you sure you want to sell ${this.quantity} ${this.chosenStock} stocks?`);
      if (isCommfirmed) {
        this.tradeService.makeTrade(tradeData).subscribe( res => {
          console.log("Sell submitted");
        })
        this.chosenQuantity -= Number(this.quantity);
        this.choiceValue = '';
        this.tracker = {ticker:this.ticker,quant: this.chosenQuantity};
    }

    }
    else{
      window.confirm("You can not sell more than you own");
    }

  }



}
