import { Component, OnInit } from '@angular/core';
import { Stocks } from 'src/app/models/Stocks';
import { TradeAdvice } from 'src/app/models/TradeAdvice';
import { ConnectToApiService } from 'src/app/services/connect-to-api.service';

@Component({
  selector: 'app-market-summary',
  templateUrl: './market-summary.component.html',
  styleUrls: ['./market-summary.component.css', '../../app.component.css']
})
export class MarketSummaryComponent implements OnInit {

  constructor(private tradeService:ConnectToApiService) { }

  ticker = '';
  chosenStock = '';
  
  advice = '';
  
  stockChoices: Stocks[];

  imgArray: any = [];

  graphUrl = '';
  getTradeAdviceURL:string = 'http://localhost:5000/advice';
  

  ngOnInit(): void {
    this.tradeService.getStocks().subscribe(stocks => {
      this.stockChoices = stocks;
    });
  }

  onStockChoiceChange(event) {
    this.ticker = this.stockChoices[event.target.value].stockTicker;
    this.tradeService.getTradeAdvice(this.ticker).subscribe(advice => {
      this.advice = advice;
    });
  }

  onChooseAdvice(event){
    if (this.ticker === '') {
      window.alert("Please ensure you selected a stock");
    }
    else {
      this.graphUrl = this.getTradeAdviceURL+'/'+this.ticker+'/graph.png'

    }
  }
}


