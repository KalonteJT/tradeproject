import { Component, OnInit } from '@angular/core';
import { ConnectToApiService } from '../../services/connect-to-api.service';
import { Indice } from '../../models/Indice';

@Component({
  selector: 'app-big-three',
  templateUrl: './big-three.component.html',
  styleUrls: ['./big-three.component.css', '../../app.component.css']
})
export class BigThreeComponent implements OnInit {

  constructor(private tradeService:ConnectToApiService) { }

  big3info: Indice[];


  ngOnInit(): void {
    this.tradeService.getBig3().subscribe(info => {
      this.big3info = info;
    })
  }

}
