import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BigThreeComponent } from './big-three.component';

describe('BigThreeComponent', () => {
  let component: BigThreeComponent;
  let fixture: ComponentFixture<BigThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BigThreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BigThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
