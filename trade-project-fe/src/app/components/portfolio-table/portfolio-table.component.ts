import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Portfolio } from 'src/app/models/Portfolio';
import { ConnectToApiService } from 'src/app/services/connect-to-api.service';

@Component({
  selector: 'app-portfolio-table',
  templateUrl: './portfolio-table.component.html',
  styleUrls: ['./portfolio-table.component.css','../../app.component.css']
})
export class PortfolioTableComponent implements OnInit {

  portfolio$: Observable<Portfolio[]>;
  portfolioEntries: Portfolio[];
  dataArray:any = [];

  constructor(private connectToApiService: ConnectToApiService) { 
  }
	ngOnInit() {
  //
  this.loadPortfolio();
  window.setInterval(() =>  
  this.loadPortfolio(), 10000);
    }

	loadPortfolio(){
		this.dataArray.length = 0;
		this.connectToApiService.getPortfolio().subscribe(entry => {
		this.portfolioEntries = entry;
	});}

}
