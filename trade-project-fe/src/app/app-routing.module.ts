import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortfolioScreenComponent } from './components/portfolio-screen/portfolio-screen.component';
import { MainContainerComponent } from './components/main-container/main-container.component';

const routes: Routes = [
  { path: 'portfolio', component: PortfolioScreenComponent },
  { path: 'trades', component: MainContainerComponent },
  { path: '**', component: MainContainerComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
