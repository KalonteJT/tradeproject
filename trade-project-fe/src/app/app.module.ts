import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MainContainerComponent } from './components/main-container/main-container.component';
import { MakeTradeComponent } from './components/make-trade/make-trade.component';
import { MarketSummaryComponent } from './components/market-summary/market-summary.component';
import { TradeHistoryComponent } from './components/trade-history/trade-history.component';
import { TableComponent } from './components/table/table.component';

import { PortfolioScreenComponent } from './components/portfolio-screen/portfolio-screen.component';
import { BigThreeComponent } from './components/big-three/big-three.component';
import { PortfolioTableComponent } from './components/portfolio-table/portfolio-table.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainContainerComponent,
    MakeTradeComponent,
    MarketSummaryComponent,
    TradeHistoryComponent,
    TableComponent,
    PortfolioScreenComponent,
    BigThreeComponent,
    PortfolioTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
